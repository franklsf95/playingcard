//
//  CardGameViewController.m
//  Matchismo
//
//  Created by Frank Luan on 11/23/13.
//  Copyright (c) 2013 Inception. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (strong, nonatomic) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@end

@implementation CardGameViewController

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (CardMatchingGame *)game
{
    if (!_game) {
        _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                  usingDeck:[self createDeck]];
    }
    return _game;
}

- (IBAction)touchCardButton:(UIButton *)sender {
    int buttonIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:buttonIndex];
    [self updateUI];
}

- (void)updateUI
{
    for (UIButton *btn in self.cardButtons) {
        int index = [self.cardButtons indexOfObject:btn];
        Card *card = [self.game cardAtIndex:index];
        
        [btn setTitle:[self titleForCard:card] forState:UIControlStateNormal];
        [btn setBackgroundImage:[self bgImageForCard:card] forState:UIControlStateNormal];
        btn.enabled = !card.isMatched;
        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    }
}

- (NSString *)titleForCard:(Card *)card {
    return card.isChosen ? card.contents : @"";
}

- (UIImage *)bgImageForCard:(Card *)card {
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"cardback"];
}

@end
