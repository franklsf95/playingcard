//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Frank Luan on 11/23/13.
//  Copyright (c) 2013 Inception. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
