//
//  main.m
//  Matchismo
//
//  Created by Frank Luan on 11/23/13.
//  Copyright (c) 2013 Inception. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
