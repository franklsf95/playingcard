//
//  Deck.h
//  CS193p
//
//  Created by luans on 11/23/13.
//  Copyright (c) 2013 luans. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;

- (Card *)drawRandomCard;

@end
