//
//  PlayingCard.h
//  CS193p
//
//  Created by luans on 11/23/13.
//  Copyright (c) 2013 luans. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (strong, nonatomic) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@end
